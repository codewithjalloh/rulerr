//
//  ViewController.swift
//  Rulerr
//
//  Created by wealthyjalloh on 11/09/2017.
//  Copyright © 2017 CWJ. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var dotNodes = [SCNNode]()
    var textNode = SCNNode()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingSessionConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    
    // touches began 
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if dotNodes.count >= 2 {
            for dot in dotNodes {
                dot.removeFromParentNode()
            }
            dotNodes = [SCNNode]()
        }
        
        if let touchLocation = touches.first?.location(in: sceneView) {
            
            let hitTestResult = sceneView.hitTest(touchLocation, types: .featurePoint)
            
            if let hitResult = hitTestResult.first {
                addDot(at: hitResult)
                
                let dotGeomerty = SCNSphere(radius: 0.005)
                let material = SCNMaterial()
                
                material.diffuse.contents = UIColor.green
                dotGeomerty.materials = [material]
                
                
                let dotNode = SCNNode(geometry: dotGeomerty)
                
                dotNode.position = SCNVector3(hitResult.worldTransform.columns.3.x,
                                              hitResult.worldTransform.columns.3.y,
                                              hitResult.worldTransform.columns.3.z)
                
                sceneView.scene.rootNode.addChildNode(dotNode)
                
                dotNodes.append(dotNode)
                
                if dotNodes.count >= 2 {
                    calculate()
                    
                }
                
            }
        }
    }
    
    
    func addDot(at hitResult: ARHitTestResult) {
        
    }
    
    
    
    // calculate the distance from start to end
    func calculate() {
        let start = dotNodes[0]
        let end = dotNodes[1]
        
        
        print(start.position)
        print(end.position)
        
        
        let distance = sqrt(
            
            pow(end.position.x - start.position.x, 2) +
            pow(end.position.y - start.position.y, 2) +
            pow(end.position.z - start.position.z, 2))
        
        print(abs(distance))
        
        
        // render distance to 2d text
        updateText(text: "\(abs(distance))", atPosition: end.position)
    }
    
    
    // update the text postition
    func updateText(text: String, atPosition position: SCNVector3) {
        textNode.removeFromParentNode()
        
        let textGeomerty = SCNText(string: text, extrusionDepth: 1.0)
        textGeomerty.firstMaterial?.diffuse.contents = UIColor.green
        
        textNode = SCNNode(geometry: textGeomerty)
        
        textNode.scale = SCNVector3(0.01, 0.01, 0.01)
        sceneView.scene.rootNode.addChildNode(textNode)
    }
}
